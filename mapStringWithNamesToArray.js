import sortNamesAlphabetically from "./sortNamesAlphabetically.js"

//Regex - Print a list of names, order by last name
function mapStringWithNamesToArray(list){

const regexSearchingWhiteSpacesAndDelimiters = /\s*(?:;|,|\.)\s*/;
const listPerson = list.split(regexSearchingWhiteSpacesAndDelimiters);

// const listNames = [];
// listPerson.forEach(
//     (element, index)=>listNames[index] = element.replace(divideNames, "$2 $1")
// )
const regexSearchingNamesAndLastNames = /([a-ząćęłóńśżź]+)\s+([a-ząćęłóńśżź]+)/gui;// /(\w+)\s+(\w+)/;
return listPerson.map(
    element => element.replace(regexSearchingNamesAndLastNames, "$2 $1").trim()
)
}

const list = "Carrie Bradshaw, Cody Maverick.Harry Potter ; Jason Bourne ;James Bond, Jason Bourne.Clarice Starling ,  Anna Karenina   .    Miranda Priestly; Izabela Łęcka . Danusia Jurandówna   ";

const names = mapStringWithNamesToArray(list);
const sortedNames = sortNamesAlphabetically(names);
console.table(sortedNames);